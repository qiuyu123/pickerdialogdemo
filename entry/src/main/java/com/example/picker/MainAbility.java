package com.example.picker;

import com.example.picker.dialog.PickerDialog;
import com.example.picker.listener.PickerdialogListener;
import com.example.picker.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TableLayout;
import ohos.agp.components.Text;

import static ohos.agp.utils.LayoutAlignment.BOTTOM;

public class MainAbility extends Ability {
    private Button button;
    private Text text;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initview();
    }

    private void initview() {
        button= (Button) findComponentById(ResourceTable.Id_mainbtn);
        text= (Text) findComponentById(ResourceTable.Id_miantext);
        if(button!=null){
            button.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    PickerDialog dialog=new PickerDialog(MainAbility.this, new PickerdialogListener() {
                        @Override
                        public void getPickerStrSuccess(String str) {
                            text.setText(str);
                        }

                        @Override
                        public void getPickerStrerror() {

                        }
                    });
                    dialog.setAlignment(BOTTOM);
                    dialog.show();

                }
            });
        }
    }
}
