package com.example.picker.dialog;

import com.example.picker.ResourceTable;
import com.example.picker.listener.PickerdialogListener;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Picker;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;


/***
 *  创建人： xuqing
 *  创建时间：2021年3月19日15:18:33
 *  类说明：底部选择器弹窗
 *
 */
public class PickerDialog extends CommonDialog implements Component.ClickedListener {
    private Ability context;
    private PickerdialogListener  listener;
    private Picker picker ;
    private Button cancelbtn,affirmbtn;
    private String getdata;
    private  String[]getStr={"星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期天"};
    Component layout;
    public PickerDialog(Ability context, PickerdialogListener  listener) {
        super(context);
        this.context=context;
        this.listener=listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        layout = LayoutScatter.getInstance(context).
                parse(ResourceTable.Layout_common_dialog, null, true);
      setTransparent(true);
       setContentCustomComponent(layout);
       initview();
    }

    private void initview() {
         picker= (Picker)layout.findComponentById(ResourceTable.Id_test_picker);
        cancelbtn= (Button) layout.findComponentById(ResourceTable.Id_cancel_btn);
        affirmbtn= (Button) layout.findComponentById(ResourceTable.Id_affirm_btn);
        cancelbtn.setClickedListener(this);
        affirmbtn.setClickedListener(this);
        picker.setDisplayedData(getStr);
        picker.setValueChangedListener(new Picker.ValueChangedListener() {
            @Override
            public void onValueChanged(Picker picker, int i, int i1) {

                System.out.println("i  -- > "+i+" i1 --- > "+ i1);
                System.out.println(getStr[i1]);

            }
        });

        picker.setValueChangedListener((picker1, oldVal, newVal) -> {
            // oldVal:上一次选择的值； newVal：最新选择的值
            System.out.println("oldVal  "+oldVal +"newVal   --- > "+newVal);
            System.out.println("getstr  ---  > "+getStr[newVal]);
            getdata=getStr[newVal];
        });

        picker.setFormatter(i -> {
            String value;
            switch (i) {
                case 0:
                    value = "Mon";
                    break;
                case 1:
                    value = "Tue";
                    break;
                case 2:
                    value = "Wed";
                    break;
                case 3:
                    value = "Thu";
                    break;
                case 4:
                    value = "Fri";
                    break;
                case 5:
                    value = "Sat";
                    break;
                case 6:
                    value = "Sun";
                    break;
                default:
                    value = "" + i;
            }
            return value;
        });

    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()){
            case ResourceTable.Id_affirm_btn:
                PickerDialog.this.hide();
                listener.getPickerStrSuccess(getdata);
                break;
            case ResourceTable.Id_cancel_btn:
               PickerDialog.this.hide();
                listener.getPickerStrerror();
                break;

            default:
                break;

        }

    }
}
